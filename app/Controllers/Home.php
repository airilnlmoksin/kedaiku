<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		$all_pekan=[
		[
			'nama' => 'Seremban',
			'gambar' => 'https://images.unsplash.com/photo-1505716619202-1193d03557c4?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTF8fG5lZ2VyaSUyMHNlbWJpbGFufGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
			'description' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Animi harum quam labore blanditiis pariatur saepe eum nihil soluta nemo, quidem eveniet perspiciatis nisi. Nulla recusandae optio, modi cupiditate eveniet minima!'
			],
			[
			'nama' => 'Gemas',
			'gambar' => 'https://images.unsplash.com/photo-1579232323293-4af4f9bb8dcf?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTN8fG5lZ2VyaSUyMHNlbWJpbGFufGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
			'description' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Animi harum quam labore blanditiis pariatur saepe eum nihil soluta nemo, quidem eveniet perspiciatis nisi. Nulla recusandae optio, modi cupiditate eveniet minima!'
			],
			[
			'nama' => 'Kuala Pilah',
			'gambar' => 'https://images.unsplash.com/photo-1615870794396-7fde1639f0d2?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MjR8fG5lZ2VyaSUyMHNlbWJpbGFufGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
			'description' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Animi harum quam labore blanditiis pariatur saepe eum nihil soluta nemo, quidem eveniet perspiciatis nisi. Nulla recusandae optio, modi cupiditate eveniet minima!'
			],
			[
			'nama' => 'Rembau',
			'gambar' => 'https://images.unsplash.com/photo-1605775888025-8c80cea3b8a8?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MzB8fG5lZ2VyaSUyMHNlbWJpbGFufGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
			'description' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Animi harum quam labore blanditiis pariatur saepe eum nihil soluta nemo, quidem eveniet perspiciatis nisi. Nulla recusandae optio, modi cupiditate eveniet minima!'
			],
			[
			'nama' => 'Port Dickson',
			'gambar' => 'https://images.unsplash.com/photo-1563699926131-d0054830f1af?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NTB8fG5lZ2VyaSUyMHNlbWJpbGFufGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
			'description' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Animi harum quam labore blanditiis pariatur saepe eum nihil soluta nemo, quidem eveniet perspiciatis nisi. Nulla recusandae optio, modi cupiditate eveniet minima!'
			]
		];

		$all_pekan[] = $pekan;
		
			return view('homepage',['all_pekan' => $all_pekan]);
	}

	function hello(){
		echo "<h1>Hello</h1>";
	}

	function welcome(){
		echo "<h1>Welcome</h1>";
	}

}
